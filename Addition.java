class Main {
	
	public static void main(String args[]) {
		
		//initialization
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		
		//logic
		int add = num1 + num2;
		
		//output
		System.out.println(num1 + " + " + num2 + " = " + add);
		System.out.println(String.format("%s + %s = %s", num1, num2, add));
		
	}
	
}