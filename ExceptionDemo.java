class Main {
	
	public static void main(String args[]) {
		
		try {
			int num1 = Integer.parseInt(args[0]);
			int num2 = Integer.parseInt(args[1]);
			
			try {
				int result = num1 / num2;
				System.out.println(String.format("Division of %s and %s is %s", num1, num2, result));
				
			} catch (ArithmeticException ae) {
				System.out.println(ae);
			}
			
			
		} catch (ArrayIndexOutOfBoundsException aio) {
			//System.out.println(aio);
			System.out.println("Something went wrong...");
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
}