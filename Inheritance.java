/**
class Bank
datamembers: balance
member functions: showBalance(), deposit(amt), withdraw(amt)
inherit: Bank in HDFC and SBI
override deposit and withdraw in SBI class
*/

class Bank {
	
	protected int balance;
	
	public Bank(int bal) {
		this.balance = bal;
		System.out.println(String.format("Account opened with balance: %s", this.balance));
	}
	
	public void showBalance() {
		System.out.println(String.format("Available Balance: %s", this.balance));
	}
	
	public void deposit(int amt) {
		System.out.println(String.format("Amount to deposit: %s", amt));
		this.balance += amt; // this.balance = this.balance + amt;
	}
	
	public void withdraw(int amt) {
		System.out.println(String.format("Amount to withdraw: %s", amt));
		this.balance -= amt; // this.balance = this.balance - amt;
	}
	
	public String toString() {
		return "This is bank object...";
	}
	
}

class HDFC extends Bank {
	
	public HDFC(int bal) {
		super(bal);
	}
	
}

class SBI extends Bank {
	
	public SBI(int bal) {
		super(bal);
	}
	
	public void deposit(int amt) {
		super.deposit(amt);
		this.balance -= 25;
		System.out.println(String.format("Deposit Charges: %s", 25));
	}
	
	public void withdraw(int amt) {
		super.withdraw(amt);
		this.balance -= 20;
		System.out.println(String.format("Withdrawal Charges: %s", 20));
	}
	
}

class Main {
	
	public static void main(String args[]) {
		Bank bobj = new Bank(1000);
		bobj.showBalance();
		bobj.deposit(500);
		bobj.showBalance();
		bobj.withdraw(300);
		bobj.showBalance();
		
		System.out.println(bobj);
		
		System.out.println("======================================");
		
		HDFC hobj = new HDFC(2000);
		hobj.showBalance();
		hobj.deposit(500);
		hobj.showBalance();
		hobj.withdraw(300);
		hobj.showBalance();
		
		System.out.println("======================================");
		
		SBI sobj = new SBI(3000);
		sobj.showBalance();
		sobj.deposit(500);
		sobj.showBalance();
		sobj.withdraw(300);
		sobj.showBalance();
		
		
	}
	
}