import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CalcServlet")
public class CalcServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CalcServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("I am in get method...");
		
		int x = 10;
		System.out.println(x); //10
		System.out.println(x++); //10 --- 10 + 1
		System.out.println(++x); //11 + 1 ===> 12 
		System.out.println((x++) + (++x)); //12 + 14 = 26 === x = 
		System.out.println(x); //14
		System.out.println((x--) - (--x)); //14 - 12 = 2
		
		System.out.println(012/2);
		
		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		
		response.getWriter().println(String.format("%s + %s = %s", num1, num2, num1 + num2));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("I am in post method...");
		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		
		response.getWriter().println(String.format("%s - %s = %s", num1, num2, num1 - num2));
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("I am in put method...");
		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		
		response.getWriter().println(String.format("%s * %s = %s", num1, num2, num1 * num2));
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("I am in delete method...");
		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		
		response.getWriter().println(String.format("%s / %s = %s", num1, num2, num1 / num2));
	}

	protected void doOptions(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("I am in options method...");
		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		
		response.getWriter().println(String.format("%s %% %s = %s", num1, num2, num1 % num2));
		response.getWriter().println(num1 + " % " + num2 + " = " + (num1 % num2));
	}

}
