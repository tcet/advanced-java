
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MyServlet() {
		super();
		System.out.println("Servlet constructor called...");
	}

	public void init(ServletConfig config) throws ServletException {
		System.out.println("Servlet Initialized...");
	}

	public void destroy() {
		System.out.println("Servlet Destroyed..");
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int num1 = Integer.parseInt(request.getParameter("num1"));
		
		int sqr = num1 * num1;
		
		System.out.println(sqr);
		
		response.getWriter().println(String.format("Square of %s is %s", num1, sqr));
	}

}
