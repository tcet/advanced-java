import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCDemo {

	public static void main(String[] args) {
		
		//Java Developer ---> Developer(User) ---> End User
		//Mech Engg (BUS) --> Driver(User) ------> Passenger
		
		String conn_str = "jdbc:mysql://localhost:3306/tcet";
		String db_unm	= "root";
		String db_pwd	= "";
		
		Connection conn = null;
		
		try {			
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(conn_str, db_unm, db_pwd);

			String select_sql = "SELECT * FROM users";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(select_sql);
			
			while(rs.next()) {
				System.out.println(rs.getInt("id"));
				System.out.println(rs.getString("name"));
				System.out.println(rs.getInt("age"));
				System.out.println(rs.getString("city"));
				System.out.println("==================");
			}
			
		} catch (Exception e) {
			
			System.out.println(e);
		}
		
		//'' "" ``
		
		
		
		
	}

}
