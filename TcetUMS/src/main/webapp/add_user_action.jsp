<%@page import="java.sql.Statement"%>
<%@include file="global_functions.jsp"%>

<% 
	authorize(session, response);

try {
	Connection conn = getConnection();
	
	String name = request.getParameter("name");
	String age = request.getParameter("age");
	String city = request.getParameter("city");
	
	String insert_sql = String.format("INSERT INTO `users` (`id`, `name`, `age`, `city`) VALUES (NULL, '%s', '%s', '%s')", name, age, city);
	
	Statement stmt = conn.createStatement();
	int result = stmt.executeUpdate(insert_sql); //DML
	
	if(result == 1) {
		response.sendRedirect("dashboard.jsp");
	} else {
		out.println("Something went wrong...");
	}
	
} catch (Exception e) {
	out.println(e);
}

%>