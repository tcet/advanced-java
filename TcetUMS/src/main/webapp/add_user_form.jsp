<jsp:include page="header.jsp"/>
<%@include file="global_functions.jsp"%>

<% 
	authorize(session, response);
%>
	<h3>Add User</h3>
	<form action="add_user_action.jsp" method="post">
		<label class="form-label">Name</label> 
		<input class="form-control" type="text" name="name"/><br>
		
		<label class="form-label">Age</label>
		<input class="form-control" type="text" name="age"/><br>
		
		<label class="form-label">City</label>
		<input class="form-control" type="text" name="city"/><br>
		
		<input class="btn btn-primary" type="submit" value="Add User"/>
		<a class="btn btn-primary" href="dashboard.jsp">Cancel</a>
	</form>
</body>
</html>