<jsp:include page="header.jsp"/>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@include file="global_functions.jsp"%>

<% 
	authorize(session, response);
%>
	<a class="btn btn-primary" href="add_user_form.jsp">Add New User</a>

	<table class="table table-striped">

		<thead>
			<th>Id</th>
			<th>Name</th>
			<th>Age</th>
			<th>City</th>
			<th>Action</th>
		</thead>

		<%
	
	try {
		Connection conn = getConnection();
		
		String select_sql = "SELECT * FROM users";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(select_sql);

		while (rs.next()) {
	%>

		<tr>
			<td><%= rs.getInt("id") %></td>
			<td><%= rs.getString("name") %></td>
			<td><%= rs.getInt("age") %></td>
			<td><%= rs.getString("city") %></td>
			<td>
				<a class="btn btn-primary" href="edit_form.jsp?id=<%= rs.getInt("id") %>">Edit</a> 
				<a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete record with name <%= rs.getString("name") %>?');"
					href="delete.jsp?id=<%= rs.getInt("id") %>">Delete</a>
			</td>
		</tr>


		<%
			//out.println(String.format("Id: %s | Name: %s | Age: %s | City: %s <br>", rs.getInt("id"), rs.getString("name"), rs.getInt("age"), rs.getString("city")));
		}

	} catch (Exception e) {

		System.out.println(e);
	}
	%>

	</table>
</body>
</html>