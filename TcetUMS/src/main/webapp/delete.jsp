<%@page import="java.sql.Statement"%>
<%@include file="global_functions.jsp" %>

<% 
	authorize(session, response);

try {
	Connection conn = getConnection();
	
	String userid = request.getParameter("id");
	String delete_sql = "DELETE FROM users WHERE id = " + userid;
	
	Statement stmt = conn.createStatement();
	int result = stmt.executeUpdate(delete_sql); //DML
	
	if(result == 1) {
		response.sendRedirect("dashboard.jsp");
	} else {
		out.println("Something went wrong...");
	}
	
} catch (Exception e) {
	out.println(e);
}

%>