<jsp:include page="header.jsp"/>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@include file="global_functions.jsp"%>

<% 
	authorize(session, response);

	String userid = request.getParameter("id");
	
	try {
		Connection conn = getConnection();
		
		String select_sql = "SELECT * FROM users WHERE id = " + userid;
		
		//out.println(select_sql);
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(select_sql);

		rs.next();
		
		String name = rs.getString("name");
		int age = rs.getInt("age");
		String city = rs.getString("city");
		
		//out.println(name);
		//out.println(age);
		//out.println(city);
%>

<body>
	<h3>Edit User</h3>
	<form action="edit_user_action.jsp" method="post">
		<input type="hidden" value="<%= userid %>" name="id"/>
		
		Name: <input type="text" name="name" value="<%= name %>"/><br>
		Age: <input type="text" name="age" value="<%= age %>"/><br>
		City: <input type="text" name="city" value="<%= city %>"/><br>
		
		<input class="btn btn-primary" type="submit" value="Update User"/>
		<a class="btn btn-primary" href="dashboard.jsp">Cancel</a>
	</form>
</body>
<% 
	} catch (Exception e) {
		
		out.println(e);
	}
%>
</html>