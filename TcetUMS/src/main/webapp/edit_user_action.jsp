<%@page import="java.sql.Statement"%>
<%@include file="global_functions.jsp"%>

<% 
	authorize(session, response);

try {
	Connection conn = getConnection();
	
	String id = request.getParameter("id");
	String name = request.getParameter("name");
	String age = request.getParameter("age");
	String city = request.getParameter("city");
	
	String update_sql = String.format("UPDATE `users` SET name = '%s', age = '%s', city = '%s' WHERE id = '%s'", name, age, city, id);
	
	//out.println(update_sql);
	
	Statement stmt = conn.createStatement();
	int result = stmt.executeUpdate(update_sql); //DML
	
	if(result == 1) {
		response.sendRedirect("dashboard.jsp");
	} else {
		out.println("Something went wrong...");
	}
	
} catch (Exception e) {
	out.println(e);
}

%>