<%@page import="org.apache.catalina.connector.Response"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<%!
	public Connection getConnection() {

		String conn_str = "jdbc:mysql://localhost:3306/tcet";
		String db_unm = "root";
		String db_pwd = "";

		Connection conn = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(conn_str, db_unm, db_pwd);

		} catch (Exception e) {
			System.out.println(e);
		}

		return conn;
	}
	

	public void authorize(HttpSession session, HttpServletResponse response) {
		
		if(session.getAttribute("username") == null) {
			try {				
				response.sendRedirect("login_form.jsp?msg=Please login to access this page");
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

%>