<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<title>TCET UMS App</title>
</head>
<body>
	<header>
		<div class="text-center">
			<h3>Tcet UMS App</h3>
		</div>
	</header>
	<div class="float-end">
		<h5>Hello <%= session.getAttribute("username") %></h5>
		<a class="btn btn-primary" href="logout.jsp">Logout</a>
	</div>