<%
	String unm = request.getParameter("username");
	String pwd = request.getParameter("password");
	
	if(unm.equals("admin") && pwd.equals("admin@123")) {
		
		session.setAttribute("username", unm); //username = admin
		
		//redirect to dashboard if credentials are correct
		response.sendRedirect("dashboard.jsp");
		
	} else {
		String msg = "Username or Password is incorrect...";
		response.sendRedirect(String.format("login_form.jsp?msg=%s", msg));
	}
%>