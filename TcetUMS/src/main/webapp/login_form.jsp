<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<title>TCET UMS App</title>
</head>
<body>

	<div style="color: red;">
		<%= request.getParameter("msg") == null ? "" : request.getParameter("msg") %>
	</div>
	
	<!-- form action="login_action.jsp" method="post">
	
		Username: <input type="text" name="username" autofocus/><br>
		
		Password: <input type="password" name="password"/><br>
		
		<input type="submit" value="Login"/>
		
	</form-->
	
	<div class="text-center">
		<center>
			<form action="login_action.jsp" method="post" style="width: 200px;">
			  <div class="mb-3">
			    <label class="form-label">Username</label>
			    <input type="text" class="form-control" name="username" autofocus/>	    
			  </div>
			  <div class="mb-3">
			    <label class="form-label">Password</label>
			    <input type="password" class="form-control" name="password"/>
			  </div>
			  <button type="submit" class="btn btn-primary">Login</button>
			</form>
		</center>
	</div>
	
</body>
</html>